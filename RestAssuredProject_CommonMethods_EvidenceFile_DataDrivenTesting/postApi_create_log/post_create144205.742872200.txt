endpoint is :
https://reqres.in/api/users

request body is:
{
    "email": "pekka@yahoo.com",
    "password": "pekka07"
}

response header date is :
Tue, 27 Feb 2024 09:12:05 GMT

response body is :
{
    "email": "pekka@yahoo.com",
    "password": "pekka07",
    "id": "159",
    "createdAt": "2024-02-27T09:12:05.092Z"
}

