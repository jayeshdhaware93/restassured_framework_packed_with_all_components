package Repositery;

import java.io.IOException;
import java.util.ArrayList;

import common_runner.commonRunner;
import common_trigger_methods.Utility;

public class RequestBody extends Data_repositery {
	public static String key_innameORemail;
	public static String key_injobORpasw ;
	public static String value_innameORemail;
	public static String value_injobORpasw;

	public static String req_post_create() throws IOException {
		ArrayList<String> inputdata = common_trigger_methods.Utility.readExcelDataFile("PATCH_API", "patch_TC5",
				"excel_data_file", ".xlsx");
		key_innameORemail = inputdata.get(1);
		value_innameORemail = inputdata.get(2);
		key_injobORpasw = inputdata.get(3);
		value_injobORpasw = inputdata.get(4);

		String req_post_create = "{\r\n" + "    \"" + key_innameORemail + "\": \"" + value_innameORemail + "\",\r\n" + "    \""
				+ key_injobORpasw + "\": \"" + value_injobORpasw + "\"\r\n" + "}";
		return req_post_create;
	}

	

	public static String req_post_reg_succ() throws IOException {
		
		
		ArrayList<String> logindata = common_trigger_methods.Utility.readExcelDataFile("POST_API", "post_TC1",
				"excel_data_file", ".xlsx");
		key_innameORemail = logindata.get(1);
		value_innameORemail = logindata.get(2);
		key_injobORpasw = logindata.get(3);
		value_injobORpasw = logindata.get(4);

//		String req_post_reg_succ = "{\r\n"
//				+ "    \""+key_innameORemail+"\": \""+value_innameORemail+"\",\r\n"
//				+ "    \""+key_injobORpasw+"\": \""+value_injobORpasw+"\"\r\n"
//				+ "}";
	  String req_post_reg_succ="{\r\n"
			+ "    \"email\": \"eve.holt@reqres.in\",\r\n"
			+ "    \"password\": \"pistol\"\r\n"
			+ "}";
		return req_post_reg_succ;
	}
	
	

	public static String req_put_update() throws IOException {
		
		
		ArrayList<String> inputdata = common_trigger_methods.Utility.readExcelDataFile("POST_API", "post_TC4", "excel_data_file", ".xlsx");
		
		key_innameORemail = inputdata.get(1);
		value_innameORemail = inputdata.get(2);
		key_injobORpasw = inputdata.get(3);
		value_injobORpasw = inputdata.get(4);
		String req_put_update = "{\r\n" + "    \""+key_innameORemail+"\": \""+value_innameORemail+"\",\r\n" + "    \""+key_injobORpasw+"\": \""+value_injobORpasw+"\"\r\n"
				+ "}";
		return req_put_update;
	}

	public static String req_patch_update() {
		String req_patch_update = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n"
				+ "}";
		return req_patch_update;

	}

}
