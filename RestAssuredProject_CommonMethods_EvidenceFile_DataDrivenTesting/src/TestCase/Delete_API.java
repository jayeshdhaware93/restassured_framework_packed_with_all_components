package TestCase;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Repositery.Data_repositery;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.restassured.response.Response;

public class Delete_API {

	public static void executor() throws IOException {
		String endpoint = Data_repositery.hostname() + Data_repositery.resource_delete();
		File dir_name = Utility.createLogDirectory("delete_API_log");
		int status_code = 0;
		Response response = Api_trigger.delete_trigger(Data_repositery.headername(), Data_repositery.headervalue(),
				endpoint);

		for (int i = 0; i < 5; i++) {
			status_code = response.getStatusCode();
			if (status_code == 204) {
				Utility.evidenceFileCreator(dir_name, Utility.testLogName("delete_API"), endpoint, null,
						response.getHeader("Date"), null);
				validator(response);
				break;

			} else {
				System.out.println("valid status code is not found at" + " " + i + "th iteration");
			}
		}
		if (status_code != 204) {
			System.out.println("valid satus code is not found at all,hence failing test case");

		}
	}

	public static void validator(Response response) {

		int Stauscode = response.getStatusCode();

		System.out.println("status code is :" + Stauscode);

//		validate status code 
		Assert.assertEquals(Stauscode, 204);

	}
}
