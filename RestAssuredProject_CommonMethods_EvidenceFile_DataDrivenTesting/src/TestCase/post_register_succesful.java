package TestCase;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Repositery.Data_repositery;
import Repositery.RequestBody;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class post_register_succesful extends Utility {

	public static void executor() throws IOException {

		File dir = Utility.createLogDirectory("post_reg_succ_log");
		String endpoint = Data_repositery.hostname() + Data_repositery.resource_reg_succ();
		String requestbody = RequestBody.req_post_reg_succ();
		int status_code = 0;
		Response response = Api_trigger.post_reg_succ_trigger(Data_repositery.headername(),
				Data_repositery.headervalue(), requestbody, endpoint);
		System.out.println(response.getStatusCode());
		for (int i = 0; i < 5; i++) {

			status_code = response.getStatusCode();
			
			if (status_code == 200) {

				Utility.evidenceFileCreator(dir, Utility.testLogName("post_reg_succ"), endpoint, requestbody,
						response.getHeader("Date"), response.getBody().asPrettyString());

				validator(response, requestbody);
				break;
			}

			else {
				System.out.println("valid status code is not found in" + " " + i + "th iteration");
			}
		}
		if (status_code != 200) {
			System.out.println("valid status code is not found a all,hence failing the test case");
		}
	}

	public static void validator(Response response, String requestbody) {

		System.out.println(response.getBody().asPrettyString());

//	fetch status code and store i to variable
		int status_code = response.getStatusCode();
		System.out.println( "status code is :"+status_code);

//		fetch request body parameters 

		System.out.println("------request body parameters--------");
		JsonPath req_jsn = new JsonPath(requestbody);
		String req_email = req_jsn.getString("email");
		System.out.println(req_email);
		String req_pasw = req_jsn.getString("password");
		System.out.println(req_pasw);

//	fetch response body

		System.out.println("-------response body parameters--------");
		String res_id = response.getBody().jsonPath().getString("id");
		System.out.println(res_id);
		String res_token = response.getBody().jsonPath().getString("token");
		System.out.println(res_token);

//	validate response body parameters and sttus code
		Assert.assertEquals(status_code, 200);
		;
		Assert.assertNotNull(res_id);
		Assert.assertNotNull(res_token);

	}
}
