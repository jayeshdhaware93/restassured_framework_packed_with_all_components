package TestCase;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Repositery.Data_repositery;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Get_user_list {
	public static void executor() throws IOException {

		String endpoint = Data_repositery.hostname() + Data_repositery.resource_get_user_list();
		File log_dir = Utility.createLogDirectory("get_API_user_list_log");
		int status_code = 0;

		Response response = Api_trigger.get_list_user_trigger(Data_repositery.headername(),
				Data_repositery.headervalue(), endpoint);
		for (int i = 0; i < 5; i++) {
			status_code = response.getStatusCode();

			if (status_code == 200) {
				Utility.evidenceFileCreator(log_dir, Utility.testLogName("get_list_user"), endpoint, null,
						response.getHeader("Date"), response.getBody().asPrettyString());
				validator(response);
				break;

			}
			else {
				System.out.println("valid response code is not found in" + " " + i + "th iteration");
			}
		}
		if (status_code != 200) {
			System.out.println("valid response code is not found at all,hence failing the the test case ");
		}

	}

	public static void validator(Response response) {

//   fetch response body and status code
		String res_body = response.getBody().asPrettyString();
		System.out.println(response.getBody().asPrettyString());
		int statusCcode = response.getStatusCode();
		System.out.println("stsus code is:" + " " + statusCcode);

// fetch response body parameters
//		create json object of response body to fetch length of data array

		JsonPath res_jsn = new JsonPath(res_body);
		int count = res_jsn.getInt("data.size()");
		System.out.println("count of data array from response:" + " " + count);

//		declare arrays of expected data

		int ids[] = { 7, 8, 9, 10, 11, 12 };
		String Emails[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String firstNames[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String lastNames[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };

//	create arrays to store fetched data from declared array	

		int idArr[] = new int[count];
		String emailArr[] = new String[count];
		String fnamesArr[] = new String[count];
		String lnamesArr[] = new String[count];

//	fetch data and store into newly created array
		for (int i = 0; i < count; i++) {
			{
				System.out.println("\n" + "---------fetched array data from arrays of expected data ---------");

				int res_id = res_jsn.getInt("data[" + i + "].id");
				System.out.println("\n" + res_id);
				idArr[i] = res_id;

				String res_email = res_jsn.get("data[" + i + "].email");
				System.out.println(res_email);
				emailArr[i] = res_email;

				String res_firstName = res_jsn.getString("data[" + i + "].first_name");
				System.out.println(res_firstName);
				fnamesArr[i] = res_firstName;

				String res_lastName = res_jsn.getString("data[" + i + "].last_name");
				System.out.println(res_lastName);
				lnamesArr[i] = res_lastName;
			}

//		validate responsebody prameters
			System.out.println("-------data from fetched data arrays to validate with expected array---------");

			System.out.println(idArr[i]);
			System.out.println(emailArr[i]);
			System.out.println(fnamesArr[i]);
			System.out.println(lnamesArr[i]);

//     validate response body parameters

			Assert.assertEquals(ids[i], idArr[i]);
			Assert.assertEquals(Emails[i], emailArr[i]);
			Assert.assertEquals(firstNames[i], fnamesArr[i]);
			Assert.assertEquals(lastNames[i], lnamesArr[i]);

		}
//		validate status code 
		Assert.assertEquals(statusCcode, 200);

	}
}
