package TestCase;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Repositery.Data_repositery;
import Repositery.RequestBody;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Put_update {

	public static void executor() throws IOException {
		String endpoint = Data_repositery.hostname() + Data_repositery.resource_put_update();
		String requestbody = RequestBody.req_put_update();
		File dir_name = Utility.createLogDirectory("put_update_API_log");
		int status_code = 0;

		Response response = Api_trigger.put_update(Data_repositery.headername(), Data_repositery.headervalue(),
				requestbody, endpoint);

		for (int i = 0; i < 5; i++) {
			status_code = response.getStatusCode();

			if (status_code == 200) {
				Utility.evidenceFileCreator(dir_name, Utility.testLogName("put_update"), endpoint, requestbody,
						response.getHeader("Date"), response.getBody().asPrettyString());
				validator(response, requestbody);
				break;
			} else {
				System.out.println("\n" + "valid response code is not found at" + " " + i + "th iteration");
			}
		}

		if (status_code != 200) {
			System.out.println("valid status code is not found at all,hence failing the test case");
		}

	}

	public static void validator(Response response, String requestbody) {

		int StatusCcode = response.getStatusCode();
		System.out.println("\n" + "status code is:" + " " + StatusCcode);
		System.out.println("\n" + "-----response body is ------");

		System.out.println(response.asPrettyString());
//		create request body object to fetch request body parameters
		JsonPath req_jsn = new JsonPath(requestbody);
		System.out.println("\n" + "-----request body parameters-------");
		String req_name = req_jsn.getString(RequestBody.key_innameORemail);
		System.out.println("name:" + req_name);
		String req_job = req_jsn.getString(RequestBody.key_injobORpasw);
		System.out.println("job:" + req_job);

//		extract response body parameters
		System.out.println("\n" + "-------response body parameters-------");

		String res_name = response.getBody().jsonPath().getString(RequestBody.key_innameORemail);
		System.out.println("name:" + res_name);
		String res_job = response.getBody().jsonPath().getString(RequestBody.key_injobORpasw);
		System.out.println("job:" + res_job);
		String res_time = response.getBody().jsonPath().getString("updatedAt");
		System.out.println("updated at:" + res_time);
		res_time = res_time.substring(0, 11);
		System.out.println(res_time);

//		get local time
		LocalDateTime curranttime = LocalDateTime.now();
		String exp_time = curranttime.toString().substring(0, 11);

//		validate response body parameters

		Assert.assertEquals(StatusCcode, 200);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_time, exp_time);

	}

}
