package common_trigger_methods;

import java.io.IOException;

import Repositery.Data_repositery;
import Repositery.RequestBody;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Api_trigger extends RequestBody {

	public static Response post_trigger(String headername, String headervalue, String requestbody, String endpoint)
			throws IOException {
		RequestSpecification req_spec = RestAssured.given();
		req_spec.header(Data_repositery.headername(), Data_repositery.headervalue());
		req_spec.body(RequestBody.req_post_create());
		Response response1 = req_spec.post(Data_repositery.hostname() + Data_repositery.resource_post_create());
		return response1;

	}

	public static Response post_reg_succ_trigger(String headername, String headervalue, String requestbody,
			String endpoint) throws IOException {
		RequestSpecification req_spec = RestAssured.given();
		req_spec.header(Data_repositery.headername(), Data_repositery.headervalue());
		req_spec.body(RequestBody.req_post_reg_succ());
		Response response2 = req_spec.post(Data_repositery.hostname() + Data_repositery.resource_reg_succ());
		return response2;
	}

	public static Response get_list_user_trigger(String headername, String headervalue, String endpoint) {
		RequestSpecification req_spec = RestAssured.given();
		req_spec.header(Data_repositery.headername(), Data_repositery.headervalue());
		Response response = req_spec.get(Data_repositery.hostname() + Data_repositery.resource_get_user_list());
		return response;
	}

	public static Response put_update(String headername, String headervalue, String requestbody, String endpoint)
			throws IOException {
		RequestSpecification req_spec = RestAssured.given();
		req_spec.header(Data_repositery.headername(), Data_repositery.headervalue());
		req_spec.body(RequestBody.req_put_update());
		Response response = req_spec.put(Data_repositery.hostname() + Data_repositery.resource_put_update());
		return response;
	}

	public static Response patch_update_trigger(String headername, String headervalue, String requesbody,
			String endpoint) {
		RequestSpecification req_spec = RestAssured.given();
		req_spec.header(Data_repositery.headername(), Data_repositery.headervalue());
		req_spec.body(RequestBody.req_patch_update());
		Response response = req_spec.patch(Data_repositery.hostname() + Data_repositery.resource_patch_update());
		return response;
	}
	public static Response  delete_trigger(String headername,String headervalue,String endpoint) {
		RequestSpecification req_spec= RestAssured.given();
		req_spec.header(Data_repositery.headername(),Data_repositery.headervalue());
		Response response = req_spec.delete(Data_repositery.hostname()+Data_repositery.resource_delete());
				return response;
	}
	public static Response get_single_user_trigger(String headername,String headervalue,String endpoint) {
		RequestSpecification req_spec= RestAssured.given();
		req_spec.header(Data_repositery.headername(),Data_repositery.headervalue());
		Response response = req_spec.get(Data_repositery.hostname()+Data_repositery.resource_get_single_user());
		return response;
	}
}
