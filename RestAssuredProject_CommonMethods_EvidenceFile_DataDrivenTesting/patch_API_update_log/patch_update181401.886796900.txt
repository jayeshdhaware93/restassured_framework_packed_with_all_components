endpoint is :
https://reqres.in/api/users/2

request body is:
{
    "name": "morpheus",
    "job": "zion resident"
}

response header date is :
Sun, 25 Feb 2024 12:44:02 GMT

response body is :
{
    "name": "morpheus",
    "job": "zion resident",
    "updatedAt": "2024-02-25T12:44:02.702Z"
}

