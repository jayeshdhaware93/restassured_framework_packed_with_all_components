# RestAssured_Framework_packed_with_All_Components

created Rest Assured framework using java programming language encapsulating all our requirements:

1.to configure all Rest APIs and : 
     > Execute

     > Extract response = used rest Assured library

     > Parse the response = used jsonpath class which belongs to rest assured library .

     > Validate the response = used testNG library.
     
2.framework is capable of reusability by creating common methods and centralised configuration files.

   a. Here we have created  repositery package which contains a data_repositery class which contains 
   common data like headername,headervalue,hostname and different resources and another class requestbody 
   which contains all requestbodies at centralised location.

   b. Created common trigger method package which has common utility class contains common utility methods
   such as log directory creation ,evidence file creation and wrting data into excel file.
   Repositery of trigger methods for all API requests.
   
   c. seperate testcase class for each API method.
   
   d. Made common running class for all API requests.     

3.Framework is able to do data driven testing by using apachePOI library for reading data from excel sheet.

4.Maven dependencies added : 
      1.rest assured
      2.testNG
      3.ApachePOI-a.poi
                  b.poi-ooxml 
                  c.poi-scratchpad 
                  d.poi-excelant
                  e.poi-examples           

Executed all APIs from Reqres free API site using given,when,then method and using request specification and response class method.


